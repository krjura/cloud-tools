node {

  emailRecipients = 'krjura@gmail.com'

  try {

    build()

    notificationBuildSuccessful()

  } catch(err) {
    notificationBuildFailed()
    throw err
  }
}

def build() {

  resolveProperties()

  withCredentials([
    usernamePassword(credentialsId: 'DOCKER_KRJURA_ORG_CREDENTIALS', usernameVariable: 'REPOSITORY_USERNAME', passwordVariable: 'REPOSITORY_PASSWORD')
  ]) {
    withEnv([
      'GRADLE_USER_HOME=/opt/cache/gradle',
      'ENVIRONMENT=jenkins']) {

        withDockerRegistry([credentialsId: 'DOCKER_KRJURA_ORG_CREDENTIALS', url: "https://docker.krjura.org"]) {

          docker.image('docker.krjura.org/cloud-tools/build-env:1').inside("-v cloud-tools-gradle-cache:${GRADLE_USER_HOME}") {

            stage('Initialize') {
              checkout scm
            }

            stage('Build, Test, Deploy') {
              sh 'gradle --no-daemon --info -p commons/cloud-tools-commons build publishProject'

              sh 'gradle --no-daemon --info -p commons/consul-client build publishProject'

              sh 'gradle --no-daemon --info -p commons/consul-config build publishProject'

              sh 'gradle --no-daemon --info -p commons/consul-discovery build publishProject'
            }
          }
        }
      }
    }
}

def notificationBuildFailed() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  mail to: emailRecipients,
    subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has FAILED",
    body: "Please go to ${BUILD_URL} for details."
}

def notificationBuildSuccessful() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  if( currentBuild.previousBuild == null ) {
    return
  }

  if (currentBuild.previousBuild.result == 'FAILURE') {
    mail to: emailRecipients,
      subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has RECOVERED",
      body: "Please go to ${BUILD_URL} for details."
  }
}

def resolveProperties() {
  def config = []

  // make sure cleanup is done on a regular basis
  config.add(
    buildDiscarder(
      logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '30', numToKeepStr: '20')))

  config.add(disableConcurrentBuilds())

  properties(config)
}
