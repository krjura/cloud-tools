package org.krjura.cloud.tools.commons.ex;

public class HttpClientException extends Exception {

    private final int code;

    public HttpClientException(int code, String message) {
        super(message);

        this.code = code;
    }

    public HttpClientException(int code, String message, Throwable e) {
        super(message, e);

        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
