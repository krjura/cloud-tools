package org.krjura.cloud.tools.commons.http;

import org.krjura.cloud.tools.commons.ex.HttpClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Objects;

public class HttpClientHelper {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientHelper.class);

    private static final String HTTP_HEADER_CONNECTION = "Connection";
    private static final String HTTP_HEADER_CONNECTION_CLOSE = "close";

    public static byte[] httpGet(String urlString, Map<String, String> headers) throws HttpClientException {
        Objects.requireNonNull(urlString);
        Objects.requireNonNull(headers);

        if( logger.isDebugEnabled() ) {
            logger.debug("calling url {} with HTTP GET", urlString);
        }

        HttpURLConnection connection = null;

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.setRequestMethod("GET");

            connection.setRequestProperty(HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTION_CLOSE);
            setHeaders(headers, connection);

            checkResponseCode(connection);

            return readSuccessResponse(connection);
        } catch ( IOException e ) {
            logger.warn("Cannot connect to url {}", urlString);
            throw new HttpClientException(500, "Cannot connect to url " + urlString, e);
        } finally {
            close(connection);
        }
    }

    public static byte[] httpPut(String urlString, String data, Map<String, String> headers)
            throws HttpClientException {

        Objects.requireNonNull(urlString);
        Objects.requireNonNull(data);

        return httpPut(urlString, data.getBytes(), headers);
    }

    public static byte[] httpPut(String urlString, byte[] data, Map<String, String> headers)
            throws HttpClientException {

        Objects.requireNonNull(urlString);
        Objects.requireNonNull(data);
        Objects.requireNonNull(headers);

        if( logger.isDebugEnabled() ) {
            logger.debug("calling url {} with HTTP POST with data", urlString);
        }

        HttpURLConnection connection = null;

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");

            connection.setRequestProperty(HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTION_CLOSE);
            setHeaders(headers, connection);

            connection.getOutputStream().write(data);
            checkResponseCode(connection);

            return readSuccessResponse(connection);
        } catch ( IOException e ) {
            logger.warn("Cannot connect to url {}", urlString);
            throw new HttpClientException(500, "Cannot connect to url " + urlString, e);
        } finally {
            close(connection);
        }
    }

    public static byte[] httpPut(String urlString, Map<String, String> headers) throws HttpClientException {
        Objects.requireNonNull(urlString);
        Objects.requireNonNull(headers);

        if( logger.isDebugEnabled() ) {
            logger.debug("calling url {} with HTTP POST with data", urlString);
        }

        HttpURLConnection connection = null;

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.setRequestMethod("PUT");

            connection.setRequestProperty(HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTION_CLOSE);
            setHeaders(headers, connection);

            checkResponseCode(connection);

            return readSuccessResponse(connection);
        } catch ( IOException e ) {
            logger.warn("Cannot connect to url {}", urlString);
            throw new HttpClientException(500, "Cannot connect to url " + urlString, e);
        } finally {
            close(connection);
        }
    }

    public static byte[] httpDelete(String urlString, Map<String, String> headers) throws HttpClientException {
        Objects.requireNonNull(urlString);
        Objects.requireNonNull(headers);

        if( logger.isDebugEnabled() ) {
            logger.debug("calling url {} with HTTP DELETE", urlString);
        }

        HttpURLConnection connection = null;

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.setRequestMethod("DELETE");

            connection.setRequestProperty(HTTP_HEADER_CONNECTION, HTTP_HEADER_CONNECTION_CLOSE);
            setHeaders(headers, connection);

            checkResponseCode(connection);

            return readSuccessResponse(connection);
        } catch ( IOException e ) {
            logger.warn("Cannot connect to url {}", urlString);
            throw new HttpClientException(500, "Cannot connect to url " + urlString, e);
        } finally {
            close(connection);
        }
    }

    private static void setHeaders(Map<String, String> headers, HttpURLConnection connection) {
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            connection.setRequestProperty(entry.getKey(), entry.getValue());
        }
    }

    private static void checkResponseCode(HttpURLConnection connection) throws HttpClientException, IOException {
        int responseCode = connection.getResponseCode();

        if (responseCode != HttpURLConnection.HTTP_OK) {
            byte[] data = readErrorResponse(connection);
            String response = new String(data);
            logger.warn("Server returned response code {}: response was \n '{}'", responseCode, response);

            throw new HttpClientException(responseCode, response);
        }
    }

    private static byte[] readErrorResponse(HttpURLConnection connection) {
        return readResponse(connection.getErrorStream());
    }

    private static byte[] readSuccessResponse(HttpURLConnection connection) {
        try {
            return readResponse(connection.getInputStream());
        } catch (IOException e) {
            logger.warn("Cannot read input stream", e);

            return new byte[0];
        }
    }

    private static byte[] readResponse(InputStream is) {
        try {
            if(is == null) {
                return new byte[0];
            }

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

            return buffer.toByteArray();
        } catch (IOException e) {
            logger.warn("Cannot read input stream", e);

            return new byte[0];
        }
    }

    private static void close(HttpURLConnection connection) {
        if(connection != null) {
            connection.disconnect();
        }
    }
}
