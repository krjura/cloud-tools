package org.krjura.cloud.tools.consul.client;

import org.krjura.cloud.tools.consul.client.impl.ConsulAgentClientImpl;
import org.krjura.cloud.tools.consul.client.impl.ConsulCatalogClientImpl;
import org.krjura.cloud.tools.consul.client.impl.ConsulHealthClientImpl;
import org.krjura.cloud.tools.consul.client.impl.ConsulKvClientImpl;
import org.krjura.cloud.tools.consul.client.impl.ConsulStatusClientImpl;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulAgentClient;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulCatalogClient;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulHealthClient;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulKvClient;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulStatusClient;
import org.krjura.cloud.tools.consul.client.utils.UrlUtils;

import java.util.Objects;

public class ConsulClient {

    private final ConsulKvClientImpl kvClient;

    private final ConsulAgentClientImpl agentClient;

    private final ConsulStatusClientImpl statusClient;

    private final ConsulHealthClientImpl healthClient;

    private final ConsulCatalogClientImpl catalogClient;

    public ConsulClient(String baseUrl, String token) {
        Objects.requireNonNull(baseUrl);
        // token can be null

       String fixedBaseUrl = UrlUtils.fixUrl(baseUrl);

       this.kvClient = new ConsulKvClientImpl(fixedBaseUrl, token);
       this.agentClient = new ConsulAgentClientImpl(fixedBaseUrl, token);
       this.statusClient = new ConsulStatusClientImpl(fixedBaseUrl, token);
       this.healthClient = new ConsulHealthClientImpl(fixedBaseUrl, token);
       this.catalogClient = new ConsulCatalogClientImpl(fixedBaseUrl, token);
    }

    public ConsulKvClient kv() {
        return this.kvClient;
    }

    public ConsulAgentClient agent() {
        return this.agentClient;
    }

    public ConsulStatusClient status() {
        return this.statusClient;
    }

    public ConsulHealthClient health() {
        return this.healthClient ;
    }

    public ConsulCatalogClient catalog() {
        return this.catalogClient;
    }
}
