package org.krjura.cloud.tools.consul.client;

import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.pojo.agent.request.RegisterRequest;

public class UsageExample {

    public static void main(String[] args) throws ConsulException {

        ConsulClient client = new ConsulClient("http://localhost:8500", "1");

        //System.out.println(client.status().raftLeader());

        //System.out.println(client.status().raftPeers());

        //System.out.println(client.agent().deRegister("service:demo"));

        RegisterRequest request = RegisterRequest.builder()
                .id("node1:demo")
                .name("demo")
                .tags("primary")
                .address("192.168.1.73")
                .port(8000)
                .meta("local", "yes")
                .enableTagOverride(false)
                .check()
                    .id("service:demo")
                    .name("service:demo")
                    .notes("demo health check")
                    .deRegisterCriticalServiceAfter("10m")
                    .http("http://localhost:8500/v1/status/leader")
                    .method("GET")
                    .interval("10s")
                    .build()
                .build();

        //System.out.println(client.agent().register(request));

        //System.out.println(client.agent().list());

        //System.out.println(client.health().passingServices("demo"));

        //System.out.println(client.catalog().serviceInstances("authentication-service"));

    }
}
