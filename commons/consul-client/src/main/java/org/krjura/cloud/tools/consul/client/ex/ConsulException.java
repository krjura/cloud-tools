package org.krjura.cloud.tools.consul.client.ex;

public class ConsulException extends Exception {

    public ConsulException(String s) {
        super(s);
    }

    public ConsulException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
