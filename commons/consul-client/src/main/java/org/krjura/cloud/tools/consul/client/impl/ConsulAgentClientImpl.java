package org.krjura.cloud.tools.consul.client.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.krjura.cloud.tools.commons.ex.HttpClientException;
import org.krjura.cloud.tools.commons.http.HttpClientHelper;
import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulAgentClient;
import org.krjura.cloud.tools.consul.client.pojo.agent.response.AgentServiceResponse;
import org.krjura.cloud.tools.consul.client.pojo.agent.response.AgentServicesResponse;
import org.krjura.cloud.tools.consul.client.pojo.agent.request.RegisterRequest;
import org.krjura.cloud.tools.consul.client.utils.ConsulUtils;
import org.krjura.cloud.tools.consul.client.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

public class ConsulAgentClientImpl implements ConsulAgentClient {

    private static final Logger logger = LoggerFactory.getLogger(ConsulAgentClientImpl.class);

    private static final String PATH_CONSUL_AGENT = "/v1/agent/";

    private final String baseUrl;

    private final String token;

    public ConsulAgentClientImpl(String baseUrl, String token) {
        Objects.requireNonNull(baseUrl);
        // token can be null

        this.baseUrl = baseUrl;
        this.token = token;
    }

    public String register(RegisterRequest registerRequest) throws ConsulException {
        Objects.requireNonNull(registerRequest);

        String url = this.baseUrl + PATH_CONSUL_AGENT + "service/register";

        return genericRequest(url, registerRequest);
    }

    public String deRegister(String serviceId) throws ConsulException {
        Objects.requireNonNull(serviceId);

        String url = this.baseUrl + PATH_CONSUL_AGENT + "service/deregister/" + serviceId;

        try {
            return new String(HttpClientHelper.httpPut(url, ConsulUtils.withToken(this.token)));
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        }
    }

    private String genericRequest(String url, Object request) throws ConsulException {
        try {
            String requestContent = JsonUtils.createObjectMapper().writeValueAsString(request);
            return new String(HttpClientHelper.httpPut(url, requestContent, ConsulUtils.withToken(this.token)));
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        } catch (JsonProcessingException e) {
            throw new ConsulException("error generating request", e);
        }
    }

    public AgentServicesResponse list() throws ConsulException {
        String url = this.baseUrl + PATH_CONSUL_AGENT + "services";

        try {
            byte[] content = HttpClientHelper.httpGet(url, ConsulUtils.withToken(this.token));

            TypeReference<Map<String, AgentServiceResponse>> typeRef =
                    new TypeReference<Map<String, AgentServiceResponse>>() {};

            return new AgentServicesResponse(JsonUtils.createObjectMapper().readValue(content, typeRef));
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        } catch (IOException e ) {
            logger.warn("Cannot parse json content", e);
            throw new ConsulException("Cannot parse json content", e);
        }

    }
}
