package org.krjura.cloud.tools.consul.client.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import org.krjura.cloud.tools.commons.ex.HttpClientException;
import org.krjura.cloud.tools.commons.http.HttpClientHelper;
import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulCatalogClient;
import org.krjura.cloud.tools.consul.client.pojo.catalog.CatalogServiceInstanceResponse;
import org.krjura.cloud.tools.consul.client.pojo.catalog.CatalogServiceInstancesResponse;
import org.krjura.cloud.tools.consul.client.pojo.catalog.CatalogServicesResponse;
import org.krjura.cloud.tools.consul.client.utils.ConsulUtils;
import org.krjura.cloud.tools.consul.client.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ConsulCatalogClientImpl implements ConsulCatalogClient {

    private static final Logger logger = LoggerFactory.getLogger(ConsulCatalogClientImpl.class);

    private static final String PATH_CONSUL_CATALOG = "/v1/catalog/";

    private final String baseUrl;

    private final String token;

    public ConsulCatalogClientImpl(String baseUrl, String token) {
        Objects.requireNonNull(baseUrl);
        // token can be null

        this.baseUrl = baseUrl;
        this.token = token;
    }

    @Override
    public CatalogServicesResponse services() throws ConsulException {

        String url = this.baseUrl + PATH_CONSUL_CATALOG + "services";

        try {
            byte[] content = HttpClientHelper.httpGet(url, ConsulUtils.withToken(this.token));

            TypeReference<Map<String, List<String>>> typeRef =
                    new TypeReference<Map<String, List<String>>>() {};

            return new CatalogServicesResponse(JsonUtils.createObjectMapper().readValue(content, typeRef));
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        } catch (IOException e ) {
            logger.warn("Cannot parse json content", e);
            throw new ConsulException("Cannot parse json content", e);
        }
    }

    @Override
    public CatalogServiceInstancesResponse serviceInstances(String instanceId) throws ConsulException {
        String url = this.baseUrl + PATH_CONSUL_CATALOG + "service/" + instanceId;

        try {
            byte[] content = HttpClientHelper.httpGet(url, ConsulUtils.withToken(this.token));

            TypeReference<List<CatalogServiceInstanceResponse>> typeRef =
                    new TypeReference<List<CatalogServiceInstanceResponse>>() {};

            return new CatalogServiceInstancesResponse(JsonUtils.createObjectMapper().readValue(content, typeRef));
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        } catch (IOException e ) {
            logger.warn("Cannot parse json content", e);
            throw new ConsulException("Cannot parse json content", e);
        }
    }
}
