package org.krjura.cloud.tools.consul.client.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import org.krjura.cloud.tools.commons.ex.HttpClientException;
import org.krjura.cloud.tools.commons.http.HttpClientHelper;
import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulHealthClient;
import org.krjura.cloud.tools.consul.client.pojo.health.response.ConsulHealthServiceInfoResponse;
import org.krjura.cloud.tools.consul.client.pojo.health.response.ConsulHealthServicesResponse;
import org.krjura.cloud.tools.consul.client.utils.ConsulUtils;
import org.krjura.cloud.tools.consul.client.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class ConsulHealthClientImpl implements ConsulHealthClient {

    private static final Logger logger = LoggerFactory.getLogger(ConsulHealthClientImpl.class);

    private static final String PATH_CONSUL_AGENT = "/v1/health/";

    private final String baseUrl;

    private final String token;

    public ConsulHealthClientImpl(final String baseUrl, String token) {
        Objects.requireNonNull(baseUrl);
        // token can be null

        this.baseUrl = baseUrl;
        this.token = token;
    }

    public ConsulHealthServicesResponse passingServices(final String name) throws ConsulException {
        String url = this.baseUrl + PATH_CONSUL_AGENT + "service/" + name + "?passing=true";

        try {
            byte[] content = HttpClientHelper.httpGet(url, ConsulUtils.withToken(this.token));

            TypeReference<List<ConsulHealthServiceInfoResponse>> typeRef =
                    new TypeReference<List<ConsulHealthServiceInfoResponse>>() {};

            return new ConsulHealthServicesResponse(JsonUtils.createObjectMapper().readValue(content, typeRef));
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        } catch (IOException e ) {
            logger.warn("Cannot parse json content", e);
            throw new ConsulException("Cannot parse json content", e);
        }
    }
}
