package org.krjura.cloud.tools.consul.client.impl;

import org.krjura.cloud.tools.commons.ex.HttpClientException;
import org.krjura.cloud.tools.commons.http.HttpClientHelper;
import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulKvClient;
import org.krjura.cloud.tools.consul.client.pojo.ConsulKvResponse;
import org.krjura.cloud.tools.consul.client.utils.ConsulUtils;

import java.util.Objects;

public class ConsulKvClientImpl implements ConsulKvClient {

    private static final String PATH_CONSUL_KV = "/v1/kv/";

    private final String baseUrl;

    private final String token;

    public ConsulKvClientImpl(String baseUrl, String token) {
        Objects.requireNonNull(baseUrl);
        // token can be null

        this.baseUrl = baseUrl;
        this.token = token;
    }

    public ConsulKvResponse one(String key) throws ConsulException {
        Objects.requireNonNull(key);

        String url = this.baseUrl + PATH_CONSUL_KV + key + "?raw=true";

        try {
            byte[] content = HttpClientHelper.httpGet(url, ConsulUtils.withToken(this.token));

            return new ConsulKvResponse(key, content);
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        }
    }

    public String set(String key, String data) throws ConsulException {
        Objects.requireNonNull(key, data);

        String url = this.baseUrl + PATH_CONSUL_KV + key;

        try {
            byte[] content = HttpClientHelper.httpPut(url, data, ConsulUtils.withToken(this.token));

            return new String(content);
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        }
    }

    public String delete(String key) throws ConsulException {
        Objects.requireNonNull(key);

        String url = this.baseUrl + PATH_CONSUL_KV + key;

        try {
            byte[] content = HttpClientHelper.httpDelete(url, ConsulUtils.withToken(this.token));

            return new String(content);
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        }
    }
}
