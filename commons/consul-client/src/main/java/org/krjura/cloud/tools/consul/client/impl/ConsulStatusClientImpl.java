package org.krjura.cloud.tools.consul.client.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import org.krjura.cloud.tools.commons.ex.HttpClientException;
import org.krjura.cloud.tools.commons.http.HttpClientHelper;
import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.interfaces.ConsulStatusClient;
import org.krjura.cloud.tools.consul.client.pojo.raft.response.ConsulRaftPeerResponse;
import org.krjura.cloud.tools.consul.client.pojo.raft.response.ConsulRaftPeersResponse;
import org.krjura.cloud.tools.consul.client.utils.ConsulUtils;
import org.krjura.cloud.tools.consul.client.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ConsulStatusClientImpl implements ConsulStatusClient {

    private static final Logger logger = LoggerFactory.getLogger(ConsulStatusClientImpl.class);

    private static final String PATH_CONSUL_STATUS = "/v1/status/";
    private static final String PEERS_SEPARATOR = ":";

    private final String baseUrl;

    private final String token;

    public ConsulStatusClientImpl(String baseUrl, String token) {
        Objects.requireNonNull(baseUrl);
        // token can be null

        this.baseUrl = baseUrl;
        this.token = token;
    }

    public ConsulRaftPeerResponse raftLeader() throws ConsulException {

        String url = this.baseUrl + PATH_CONSUL_STATUS + "leader";

        try {
            byte[] content = HttpClientHelper.httpGet(url, ConsulUtils.withToken(this.token));
            String leader = new String(content);
            String[] parts  = leader.split(PEERS_SEPARATOR);

            if(parts.length != 2) {
                throw new ConsulException("Invalid peer: " + leader);
            }

            return new ConsulRaftPeerResponse(parts[0], parts[1]);
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        }
    }

    public ConsulRaftPeersResponse raftPeers() throws ConsulException {

        String url = this.baseUrl + PATH_CONSUL_STATUS + "peers";

        try {
            byte[] content = HttpClientHelper.httpGet(url, ConsulUtils.withToken(this.token));

            TypeReference<List<String>> typeRef = new TypeReference<List<String>>() {};

            List<String> peers = JsonUtils.createObjectMapper().readValue(content, typeRef);

            return new ConsulRaftPeersResponse(
                    peers
                    .stream()
                    .map(peer -> peer.split(PEERS_SEPARATOR))
                    .filter(peerParts -> peerParts.length == 2)
                    .map(peerParts -> new ConsulRaftPeerResponse(peerParts[0], peerParts[1]))
                    .collect(Collectors.toList())
            );
        } catch (HttpClientException e) {
            throw new ConsulException("Cannot fetch data from consul", e);
        } catch (IOException e ) {
            logger.warn("Cannot parse json content", e);
            throw new ConsulException("Cannot parse json content", e);
        }
    }
}
