package org.krjura.cloud.tools.consul.client.interfaces;

import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.pojo.agent.response.AgentServicesResponse;
import org.krjura.cloud.tools.consul.client.pojo.agent.request.RegisterRequest;

public interface ConsulAgentClient {

    String register(RegisterRequest registerRequest) throws ConsulException;

    String deRegister(String serviceId) throws ConsulException;

    AgentServicesResponse list() throws ConsulException;

}
