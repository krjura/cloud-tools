package org.krjura.cloud.tools.consul.client.interfaces;

import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.pojo.catalog.CatalogServiceInstancesResponse;
import org.krjura.cloud.tools.consul.client.pojo.catalog.CatalogServicesResponse;

public interface ConsulCatalogClient {

    CatalogServicesResponse services() throws ConsulException;

    CatalogServiceInstancesResponse serviceInstances(String instanceId) throws ConsulException;
}
