package org.krjura.cloud.tools.consul.client.interfaces;

import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.pojo.health.response.ConsulHealthServicesResponse;

public interface ConsulHealthClient {

    ConsulHealthServicesResponse passingServices(final String name) throws ConsulException;

}
