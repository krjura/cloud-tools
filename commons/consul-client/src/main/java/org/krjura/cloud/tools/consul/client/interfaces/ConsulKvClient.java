package org.krjura.cloud.tools.consul.client.interfaces;

import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.pojo.ConsulKvResponse;

public interface ConsulKvClient {

    ConsulKvResponse one(String key) throws ConsulException;

    String set(String key, String data) throws ConsulException;

    String delete(String key) throws ConsulException;
}
