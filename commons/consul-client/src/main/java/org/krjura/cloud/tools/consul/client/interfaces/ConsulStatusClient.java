package org.krjura.cloud.tools.consul.client.interfaces;

import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.pojo.raft.response.ConsulRaftPeerResponse;
import org.krjura.cloud.tools.consul.client.pojo.raft.response.ConsulRaftPeersResponse;

public interface ConsulStatusClient {

    ConsulRaftPeerResponse raftLeader() throws ConsulException;

    ConsulRaftPeersResponse raftPeers() throws ConsulException;

}
