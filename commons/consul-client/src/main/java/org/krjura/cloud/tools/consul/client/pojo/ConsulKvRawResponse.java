package org.krjura.cloud.tools.consul.client.pojo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsulKvRawResponse {

    private final Integer lockIndex;

    private final String key;

    private final Integer flags;

    private final String value;

    private final Integer createIndex;

    private final Integer modifyIndex;

    @JsonCreator
    public ConsulKvRawResponse(
            @JsonProperty("LockIndex") Integer lockIndex,
            @JsonProperty("Key") String key,
            @JsonProperty("Flags") Integer flags,
            @JsonProperty("Value") String value,
            @JsonProperty("CreateIndex") Integer createIndex,
            @JsonProperty("ModifyIndex") Integer modifyIndex) {

        this.lockIndex = lockIndex;
        this.key = key;
        this.flags = flags;
        this.value = value;
        this.createIndex = createIndex;
        this.modifyIndex = modifyIndex;
    }

    public Integer getLockIndex() {
        return lockIndex;
    }

    public String getKey() {
        return key;
    }

    public Integer getFlags() {
        return flags;
    }

    public String getValue() {
        return value;
    }

    public Integer getCreateIndex() {
        return createIndex;
    }

    public Integer getModifyIndex() {
        return modifyIndex;
    }

    @Override
    public String toString() {
        return "ConsulKv{" +
                "lockIndex=" + lockIndex +
                ", key='" + key + '\'' +
                ", flags=" + flags +
                ", value='" + value + '\'' +
                ", createIndex=" + createIndex +
                ", modifyIndex=" + modifyIndex +
                '}';
    }
}
