package org.krjura.cloud.tools.consul.client.pojo;

import java.util.Arrays;
import java.util.Objects;

public class ConsulKvResponse {

    private final String key;

    private final byte[] value;

    public ConsulKvResponse(String key, byte[] value) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(value);

        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public byte[] getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "ConsulKvResponse{" +
                "key='" + key + '\'' +
                ", value=" + Arrays.toString(value) +
                '}';
    }
}
