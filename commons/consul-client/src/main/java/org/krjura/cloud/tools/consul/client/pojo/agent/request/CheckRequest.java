package org.krjura.cloud.tools.consul.client.pojo.agent.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CheckRequest {

    @JsonProperty("Name")
    private String name;

    @JsonProperty("ID")
    private String id;

    @JsonProperty("Interval")
    private String interval;

    @JsonProperty("Notes")
    private String notes;

    @JsonProperty("DeregisterCriticalServiceAfter")
    private String deRegisterCriticalServiceAfter ;

    @JsonProperty("Args")
    private List<String> args ;

    @JsonProperty("DockerContainerID")
    private String dockerContainerId;

    @JsonProperty("GRPC")
    private String grpc;

    @JsonProperty("GRPCUseTLS")
    private boolean grpcUseTLS;

    @JsonProperty("HTTP")
    private String http;

    @JsonProperty("Method")
    private String method;

    @JsonProperty("Header")
    private Map<String, String> header;

    @JsonProperty("Timeout")
    private String timeout;

    @JsonProperty("TLSSkipVerify")
    private String tlsSkipVerify;

    @JsonProperty("TTL")
    private String ttl;

    @JsonProperty("ServiceID")
    private String serviceId;

    @JsonProperty("Status")
    private String status;

    public CheckRequest() {

    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getInterval() {
        return interval;
    }

    public String getNotes() {
        return notes;
    }

    public String getDeRegisterCriticalServiceAfter() {
        return deRegisterCriticalServiceAfter;
    }

    public List<String> getArgs() {
        return args;
    }

    public String getDockerContainerId() {
        return dockerContainerId;
    }

    public String getGrpc() {
        return grpc;
    }

    public boolean isGrpcUseTLS() {
        return grpcUseTLS;
    }

    public String getHttp() {
        return http;
    }

    public String getMethod() {
        return method;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public String getTimeout() {
        return timeout;
    }

    public String getTlsSkipVerify() {
        return tlsSkipVerify;
    }

    public String getTtl() {
        return ttl;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getStatus() {
        return status;
    }

    public static CheckRequestBuilder builder(RegisterRequest.AgentRegisterRequestBuilder parent) {
        return new CheckRequestBuilder(parent);
    }

    public static class CheckRequestBuilder {

        private RegisterRequest.AgentRegisterRequestBuilder  parent;

        private CheckRequest instance;

        public CheckRequestBuilder(RegisterRequest.AgentRegisterRequestBuilder  parent) {
            this.parent = parent;
            this.instance = new CheckRequest();
        }

        public CheckRequestBuilder name(final String name) {
            this.instance.name = name;

            return this;
        }

        public CheckRequestBuilder id(final String id) {
            this.instance.id = id;

            return this;
        }

        public CheckRequestBuilder interval(final String interval) {
            this.instance.interval = interval;

            return this;
        }

        public CheckRequestBuilder notes(final String notes) {
            this.instance.notes = notes;

            return this;
        }

        public CheckRequestBuilder deRegisterCriticalServiceAfter(final String deRegisterCriticalServiceAfter) {
            this.instance.deRegisterCriticalServiceAfter = deRegisterCriticalServiceAfter;

            return this;
        }

        public CheckRequestBuilder args(final List<String> args) {
            this.instance.args = args == null ? new ArrayList<>() : args;

            return this;
        }

        public CheckRequestBuilder dockerContainerId(final String dockerContainerId) {
            this.instance.dockerContainerId = dockerContainerId;

            return this;
        }

        public CheckRequestBuilder grpc(final String grpc) {
            this.instance.grpc = grpc;

            return this;
        }

        public CheckRequestBuilder grpcUseTLS(final boolean grpcUseTLS) {
            this.instance.grpcUseTLS = grpcUseTLS;

            return this;
        }

        public CheckRequestBuilder http(final String http) {
            this.instance.http = http;

            return this;
        }

        public CheckRequestBuilder method(final String method) {
            this.instance.method = method;

            return this;
        }

        public CheckRequestBuilder header(final Map<String, String> header) {
            this.instance.header = header == null ? new HashMap<>() : header;

            return this;
        }

        public CheckRequestBuilder timeout(final String timeout) {
            this.instance.timeout = timeout;

            return this;
        }

        public CheckRequestBuilder tlsSkipVerify(final String tlsSkipVerify) {
            this.instance.tlsSkipVerify = tlsSkipVerify;

            return this;
        }

        public CheckRequestBuilder ttl(final String ttl) {
            this.instance.ttl = ttl;

            return this;
        }

        public CheckRequestBuilder serviceId(final String serviceId) {
            this.instance.serviceId = serviceId;

            return this;
        }

        public CheckRequestBuilder status(final String status) {
            this.instance.status = status;

            return this;
        }

        public RegisterRequest.AgentRegisterRequestBuilder build() {
            Objects.requireNonNull(this.instance.name);

            return this.parent.check(this.instance);
        }
    }
}