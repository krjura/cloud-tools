package org.krjura.cloud.tools.consul.client.pojo.agent.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class RegisterRequest {

    @JsonProperty("ID")
    private String id;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Tags")
    private List<String> tags;

    @JsonProperty("Address")
    private String address;

    @JsonProperty("Port")
    private Integer port;

    @JsonProperty("Meta")
    private Map<String, String> meta;

    @JsonProperty("EnableTagOverride")
    private boolean enableTagOverride;

    @JsonProperty("Check")
    private CheckRequest check;

    private RegisterRequest() {
        this.tags = new ArrayList<>();
        this.meta  = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getAddress() {
        return address;
    }

    public Integer getPort() {
        return port;
    }

    public Map<String, String> getMeta() {
        return meta;
    }

    public boolean isEnableTagOverride() {
        return enableTagOverride;
    }

    public CheckRequest getCheck() {
        return check;
    }

    // builder for this request

    public static AgentRegisterRequestBuilder builder() {
        return new AgentRegisterRequestBuilder();
    }

    public static class AgentRegisterRequestBuilder {

        private RegisterRequest instance;

        AgentRegisterRequestBuilder() {
            this.instance = new RegisterRequest();
        }

        public AgentRegisterRequestBuilder id(final String id) {
            this.instance.id = id;

            return this;
        }

        public AgentRegisterRequestBuilder name(final String name) {
            this.instance.name = name;

            return this;
        }

        public AgentRegisterRequestBuilder tags(final List<String> tags) {
            this.instance.tags = tags == null ? new ArrayList<>() : tags;

            return this;
        }

        public AgentRegisterRequestBuilder tags(final String tag) {
            this.instance.tags.add(tag);

            return this;
        }

        public AgentRegisterRequestBuilder address(final String address) {
            this.instance.address = address;

            return this;
        }

        public AgentRegisterRequestBuilder port(final Integer port) {
            this.instance.port = port;

            return this;
        }

        public AgentRegisterRequestBuilder meta(final Map<String, String> meta) {
            this.instance.meta = meta == null ? new HashMap<>() : meta;

            return this;
        }

        public AgentRegisterRequestBuilder meta(final String key, String value) {
            Objects.requireNonNull(key);
            Objects.requireNonNull(value);

            this.instance.meta.put(key, value);

            return this;
        }

        public AgentRegisterRequestBuilder enableTagOverride(final boolean enableTagOverride) {
            this.instance.enableTagOverride = enableTagOverride;

            return this;
        }

        public AgentRegisterRequestBuilder check(final CheckRequest check) {
            Objects.requireNonNull(check);

            this.instance.check = check;

            return this;
        }

        public CheckRequest.CheckRequestBuilder check() {
            return CheckRequest.builder(this);
        }

        public RegisterRequest build() {
            Objects.requireNonNull(this.instance.name);
            Objects.requireNonNull(this.instance.port);

            return this.instance;
        }
    }

    @Override
    public String toString() {
        return "RegisterRequest{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", tags=" + tags +
                ", address='" + address + '\'' +
                ", port=" + port +
                ", meta=" + meta +
                ", enableTagOverride=" + enableTagOverride +
                '}';
    }
}