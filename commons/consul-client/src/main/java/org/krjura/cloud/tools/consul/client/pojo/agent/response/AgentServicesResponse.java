package org.krjura.cloud.tools.consul.client.pojo.agent.response;

import java.util.Map;
import java.util.Objects;

public class AgentServicesResponse {

    private final Map<String, AgentServiceResponse> services;

    public AgentServicesResponse(Map<String, AgentServiceResponse> services) {
        Objects.requireNonNull(services);

        this.services = services;
    }

    public Map<String, AgentServiceResponse> getServices() {
        return services;
    }

    @Override
    public String toString() {
        return "AgentServicesResponse{" +
                "services=" + services +
                '}';
    }
}
