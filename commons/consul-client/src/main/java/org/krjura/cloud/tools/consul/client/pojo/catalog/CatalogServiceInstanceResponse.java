package org.krjura.cloud.tools.consul.client.pojo.catalog;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CatalogServiceInstanceResponse {

    private final String id;

    private final String node;

    private final String address;

    private final String datacenter;

    private final Map<String, String> taggedAddresses;

    private final Map<String, String> nodeMeta;

    private final String serviceAddress;

    private final String serviceEnableTagOverride;

    private final String serviceId;

    private final String serviceName;

    private final Integer servicePort;

    private final Map<String, String> serviceMeta;

    private final List<String> serviceTags;

    @JsonCreator
    public CatalogServiceInstanceResponse(
            @JsonProperty("ID") String id,
            @JsonProperty("Node") String node,
            @JsonProperty("Address") String address,
            @JsonProperty("Datacenter") String datacenter,
            @JsonProperty("TaggedAddresses") Map<String, String> taggedAddresses,
            @JsonProperty("NodeMeta") Map<String, String> nodeMeta,
            @JsonProperty("ServiceAddress") String serviceAddress,
            @JsonProperty("ServiceEnableTagOverride") String serviceEnableTagOverride,
            @JsonProperty("ServiceID") String serviceId,
            @JsonProperty("ServiceName") String serviceName,
            @JsonProperty("ServicePort") Integer servicePort,
            @JsonProperty("ServiceMeta") Map<String, String> serviceMeta,
            @JsonProperty("ServiceTags") List<String> serviceTags) {

        this.id = id;
        this.node = node;
        this.address = address;
        this.datacenter = datacenter;
        this.taggedAddresses = taggedAddresses;
        this.nodeMeta = nodeMeta;
        this.serviceAddress = serviceAddress;
        this.serviceEnableTagOverride = serviceEnableTagOverride;
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.servicePort = servicePort;
        this.serviceMeta = serviceMeta;
        this.serviceTags = serviceTags;
    }

    public String getId() {
        return id;
    }

    public String getNode() {
        return node;
    }

    public String getAddress() {
        return address;
    }

    public String getDatacenter() {
        return datacenter;
    }

    public Map<String, String> getTaggedAddresses() {
        return taggedAddresses;
    }

    public Map<String, String> getNodeMeta() {
        return nodeMeta;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public String getServiceEnableTagOverride() {
        return serviceEnableTagOverride;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public Integer getServicePort() {
        return servicePort;
    }

    public Map<String, String> getServiceMeta() {
        return serviceMeta;
    }

    public List<String> getServiceTags() {
        return serviceTags;
    }

    @Override
    public String toString() {
        return "CatalogServiceInstanceResponse{" +
                "id='" + id + '\'' +
                ", node='" + node + '\'' +
                ", address='" + address + '\'' +
                ", datacenter='" + datacenter + '\'' +
                ", taggedAddresses=" + taggedAddresses +
                ", nodeMeta=" + nodeMeta +
                ", serviceAddress='" + serviceAddress + '\'' +
                ", serviceEnableTagOverride='" + serviceEnableTagOverride + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", servicePort=" + servicePort +
                ", serviceMeta=" + serviceMeta +
                ", serviceTags=" + serviceTags +
                '}';
    }
}
