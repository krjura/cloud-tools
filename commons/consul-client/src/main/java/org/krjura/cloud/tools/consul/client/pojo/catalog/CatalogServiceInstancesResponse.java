package org.krjura.cloud.tools.consul.client.pojo.catalog;

import java.util.List;

public class CatalogServiceInstancesResponse {

    private final List<CatalogServiceInstanceResponse> instances;

    public CatalogServiceInstancesResponse(List<CatalogServiceInstanceResponse> instances) {
        this.instances = instances;
    }

    public List<CatalogServiceInstanceResponse> getInstances() {
        return instances;
    }

    @Override
    public String toString() {
        return "CatalogServiceInstancesResponse{" +
                "instances=" + instances +
                '}';
    }
}
