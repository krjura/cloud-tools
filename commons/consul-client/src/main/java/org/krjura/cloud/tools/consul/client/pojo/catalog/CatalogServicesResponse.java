package org.krjura.cloud.tools.consul.client.pojo.catalog;

import java.util.List;
import java.util.Map;

public class CatalogServicesResponse {

    private final Map<String, List<String>> services;

    public CatalogServicesResponse(Map<String, List<String>> services) {
        this.services = services;
    }

    public Map<String, List<String>> getServices() {
        return services;
    }

    @Override
    public String toString() {
        return "CatalogServicesResponse{" +
                "services=" + services +
                '}';
    }
}
