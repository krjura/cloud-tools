package org.krjura.cloud.tools.consul.client.pojo.health.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsulHealthCheckResponse {

    private final String node;

    private final String checkId;

    private final String name;

    private final String status;

    private final String notes;

    private final String output;

    private final String serviceId;

    private final String serviceName;

    private final List<String> serviceTags;

    @JsonCreator
    public ConsulHealthCheckResponse(
            @JsonProperty("Node") String node,
            @JsonProperty("CheckID") String checkId,
            @JsonProperty("Name") String name,
            @JsonProperty("Status") String status,
            @JsonProperty("Notes") String notes,
            @JsonProperty("Output") String output,
            @JsonProperty("ServiceID") String serviceId,
            @JsonProperty("ServiceName") String serviceName,
            @JsonProperty("ServiceTags") List<String> serviceTags) {

        this.node = node;
        this.checkId = checkId;
        this.name = name;
        this.status = status;
        this.notes = notes;
        this.output = output;
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.serviceTags = serviceTags;
    }

    public String getNode() {
        return node;
    }

    public String getCheckId() {
        return checkId;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getNotes() {
        return notes;
    }

    public String getOutput() {
        return output;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public List<String> getServiceTags() {
        return serviceTags;
    }

    @Override
    public String toString() {
        return "ConsulHealthCheckResponse{" +
                "node='" + node + '\'' +
                ", checkId='" + checkId + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", notes='" + notes + '\'' +
                ", output='" + output + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", serviceTags=" + serviceTags +
                '}';
    }
}