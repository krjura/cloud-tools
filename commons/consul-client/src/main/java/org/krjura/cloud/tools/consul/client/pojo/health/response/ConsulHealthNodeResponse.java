package org.krjura.cloud.tools.consul.client.pojo.health.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsulHealthNodeResponse {

    private final String id;

    private final String node;

    private final String address;

    private final String datacenter;

    private final Map<String, String> taggedAddresses;

    private final Map<String, String> meta;

    @JsonCreator
    public ConsulHealthNodeResponse(
            @JsonProperty("ID") String id,
            @JsonProperty("Node") String node,
            @JsonProperty("Address") String address,
            @JsonProperty("Datacenter") String datacenter,
            @JsonProperty("taggedAddresses") Map<String, String> taggedAddresses,
            @JsonProperty("meta") Map<String, String> meta) {

        this.id = id;
        this.node = node;
        this.address = address;
        this.datacenter = datacenter;
        this.taggedAddresses = taggedAddresses == null ? new HashMap<>() : taggedAddresses;
        this.meta = meta == null ? new HashMap<>() : meta;
    }

    public String getId() {
        return id;
    }

    public String getNode() {
        return node;
    }

    public String getAddress() {
        return address;
    }

    public String getDatacenter() {
        return datacenter;
    }

    public Map<String, String> getTaggedAddresses() {
        return taggedAddresses;
    }

    public Map<String, String> getMeta() {
        return meta;
    }

    @Override
    public String toString() {
        return "ConsulHealthNodeResponse{" +
                "id='" + id + '\'' +
                ", node='" + node + '\'' +
                ", address='" + address + '\'' +
                ", datacenter='" + datacenter + '\'' +
                ", taggedAddresses=" + taggedAddresses +
                ", meta=" + meta +
                '}';
    }
}
