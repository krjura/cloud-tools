package org.krjura.cloud.tools.consul.client.pojo.health.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsulHealthServiceInfoResponse {

    private final ConsulHealthNodeResponse node;

    private final ConsulHealthServiceResponse service;

    private final List<ConsulHealthCheckResponse> checks;

    @JsonCreator
    public ConsulHealthServiceInfoResponse(
            @JsonProperty("Node") ConsulHealthNodeResponse node,
            @JsonProperty("Service") ConsulHealthServiceResponse service,
            @JsonProperty("Checks") List<ConsulHealthCheckResponse> checks) {

        Objects.requireNonNull(node);
        Objects.requireNonNull(service);
        Objects.requireNonNull(checks);

        this.node = node;
        this.service = service;
        this.checks = checks;
    }

    public ConsulHealthServiceResponse getService() {
        return service;
    }

    public ConsulHealthNodeResponse getNode() {
        return node;
    }

    public List<ConsulHealthCheckResponse> getChecks() {
        return checks;
    }

    @Override
    public String toString() {
        return "ConsulHealthServiceInfoResponse{" +
                "node=" + node +
                ",service=" + service +
                ",checks=" + checks +
                '}';
    }
}
