package org.krjura.cloud.tools.consul.client.pojo.health.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsulHealthServiceResponse {

    private final String id;

    private final String service;

    private final List<String> tags;

    private final String address;

    private final Map<String, String> meta;

    private final Integer port;

    private final boolean enableTagOverride;

    @JsonCreator
    public ConsulHealthServiceResponse(
            @JsonProperty("ID") String id,
            @JsonProperty("Service") String service,
            @JsonProperty("Tags") List<String> tags,
            @JsonProperty("Address") String address,
            @JsonProperty("Meta") Map<String, String> meta,
            @JsonProperty("Port") Integer port,
            @JsonProperty("EnableTagOverride") boolean enableTagOverride) {

        this.id = id;
        this.service = service;
        this.tags = tags == null ? new ArrayList<>() : tags;
        this.address = address;
        this.meta = meta == null ? new HashMap<>() : meta;
        this.port = port;
        this.enableTagOverride = enableTagOverride;
    }

    public String getId() {
        return id;
    }

    public String getService() {
        return service;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getAddress() {
        return address;
    }

    public Map<String, String> getMeta() {
        return meta;
    }

    public Integer getPort() {
        return port;
    }

    public boolean isEnableTagOverride() {
        return enableTagOverride;
    }

    @Override
    public String toString() {
        return "ConsulHealthServiceResponse{" +
                "id='" + id + '\'' +
                ", service='" + service + '\'' +
                ", tags=" + tags +
                ", address='" + address + '\'' +
                ", meta=" + meta +
                ", port=" + port +
                ", enableTagOverride=" + enableTagOverride +
                '}';
    }
}