package org.krjura.cloud.tools.consul.client.pojo.health.response;

import java.util.List;
import java.util.Objects;

public class ConsulHealthServicesResponse {

    private final List<ConsulHealthServiceInfoResponse> services;

    public ConsulHealthServicesResponse(List<ConsulHealthServiceInfoResponse> services) {
        Objects.requireNonNull(services);

        this.services = services;
    }

    public List<ConsulHealthServiceInfoResponse> getServices() {
        return services;
    }

    @Override
    public String toString() {
        return "ConsulHealthServicesResponse{" +
                "services=" + services +
                '}';
    }
}
