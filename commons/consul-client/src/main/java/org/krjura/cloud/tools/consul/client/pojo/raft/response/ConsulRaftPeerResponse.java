package org.krjura.cloud.tools.consul.client.pojo.raft.response;

import java.util.Objects;

public class ConsulRaftPeerResponse {

    private final String ip;

    private final String port;

    public ConsulRaftPeerResponse(String ip, String port) {
        Objects.requireNonNull(ip);
        Objects.requireNonNull(port);

        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public String getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "ConsulRaftPeerResponse{" +
                "ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                '}';
    }
}
