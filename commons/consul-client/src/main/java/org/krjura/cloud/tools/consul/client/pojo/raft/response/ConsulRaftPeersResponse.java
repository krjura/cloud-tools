package org.krjura.cloud.tools.consul.client.pojo.raft.response;

import java.util.List;
import java.util.Objects;

public class ConsulRaftPeersResponse {

    private final List<ConsulRaftPeerResponse> peers;

    public ConsulRaftPeersResponse(List<ConsulRaftPeerResponse> peers) {
        Objects.requireNonNull(peers);

        this.peers = peers;
    }

    public List<ConsulRaftPeerResponse> getPeers() {
        return peers;
    }

    @Override
    public String toString() {
        return "ConsulRaftPeersResponse{" +
                "peers=" + peers +
                '}';
    }
}
