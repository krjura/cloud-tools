package org.krjura.cloud.tools.consul.client.utils;

import java.util.Collections;
import java.util.Map;

public class ConsulUtils {

    private static final String X_CONSUL_TOKEN = "X-Consul-Token";

    private ConsulUtils() {
        // utils
    }

    public static Map<String, String> withToken(String token) {
        if(token == null || token.isEmpty()) {
            return Collections.emptyMap();
        }

        return Collections.singletonMap(X_CONSUL_TOKEN, token);
    }
}
