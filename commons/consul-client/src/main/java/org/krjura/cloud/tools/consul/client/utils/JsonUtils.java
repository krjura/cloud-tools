package org.krjura.cloud.tools.consul.client.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {

    private static ObjectMapper objectMapper;

    private JsonUtils() {
        // utils
    }

    public static ObjectMapper createObjectMapper() {
        if(objectMapper == null) {
            objectMapper = new ObjectMapper();
        }

        return objectMapper;
    }
}
