package org.krjura.cloud.tools.consul.client.utils;

public class UrlUtils {

    private UrlUtils() {
        // util
    }

    public static String fixUrl(String url) {
        if(url.endsWith("/")) {
            return url.substring(0, url.length() -1);
        } else {
            return url;
        }
    }
}
