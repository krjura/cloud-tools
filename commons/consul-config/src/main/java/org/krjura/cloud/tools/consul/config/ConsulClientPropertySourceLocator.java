package org.krjura.cloud.tools.consul.config;

import org.krjura.cloud.tools.consul.client.ConsulClient;
import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.pojo.ConsulKvResponse;
import org.krjura.cloud.tools.consul.config.spring.ConsulClientConfig;
import org.krjura.cloud.tools.consul.config.spring.pojo.ConfigurationResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.env.PropertiesPropertySourceLoader;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.cloud.bootstrap.config.PropertySourceLocator;
import org.springframework.core.env.CompositePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.ByteArrayResource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ConsulClientPropertySourceLocator implements PropertySourceLocator {

    private static final Logger logger = LoggerFactory.getLogger(ConsulClientPropertySourceLocator.class);

    private static final String CONST_LOCATIONS_SEPARATOR = ",";
    private static final String CONST_BASE_PATHS_SEPARATOR = ",";

    private static final String CONST_POSTFIX_YML = ".yml";
    private static final String CONST_POSTFIX_YAML2 = ".yaml";

    private final ConsulClientConfig consulClientConfig;

    public ConsulClientPropertySourceLocator(
            ConsulClientConfig consulClientConfig) {

        this.consulClientConfig = consulClientConfig;
    }

    @Override
    public PropertySource<?> locate(Environment environment) {
        Objects.requireNonNull(environment);

        logger.info("Trying to locate bootstrap source");

        List<String> baseUrls = parseBaseUrl(this.consulClientConfig.getBaseUrls());
        List<String> configurationFiles = parseLocations(this.consulClientConfig.getLocations());

        Objects.requireNonNull(baseUrls);
        Objects.requireNonNull(configurationFiles);

        downloadResources(baseUrls);

        return fetchConfigurations(baseUrls, configurationFiles);
    }

    private PropertySource<?>  fetchConfigurations(List<String> baseUrls, List<String> configurationFiles) {
        CompositePropertySource propertySource = new CompositePropertySource("consul-config");

        for(String configurationFile : configurationFiles) {
            fetchConfigurationFileWithRetry(baseUrls, configurationFile)
                    .forEach(propertySource::addFirstPropertySource);
        }

        return propertySource;
    }

    private List<PropertySource<?>> fetchConfigurationFileWithRetry(List<String> baseUrls, String path) {

        int numberOfRetries = this.consulClientConfig.getRetry().getNumberOfRetries();
        int baseUrlsSize = baseUrls.size();

        for(int i = 0; i < numberOfRetries; i++) {

            try {
                String baseUrl = baseUrls.get(i % baseUrlsSize);
                ConsulClient client = new ConsulClient(baseUrl, this.consulClientConfig.getToken());

                logger.info("trying path {} with basePath {}. This is attempt {}", baseUrl, path, i);
                return fetchConfigurationFile(client, path);
            } catch (ConsulException e) {
                logger.warn("Cannot fetch data from path " + path, e);

                if(this.consulClientConfig.getRetry().isRetryWhenFailed()) {
                    sleep();
                } else {
                    // retry should not be made so quit badly
                    throw new ConsulConfigurationException("Cannot download properties from path " + path);
                }
            }
        }

        throw new ConsulConfigurationException(
                "Reached maximum number of retries but still failed to load config at path " + path);
    }

    private List<PropertySource<?>> fetchConfigurationFile(ConsulClient client, String path)
            throws ConsulException {

        try {
            ConsulKvResponse response = client.kv().one(path);

            if(path.endsWith(CONST_POSTFIX_YML) || path.endsWith(CONST_POSTFIX_YAML2)) {
                return loadYmlFile(path, response);
            } else {
                return loadPropertiesFile(path, response);
            }
        } catch (IOException e) {
            throw new ConsulException("Cannot process properties", e);
        }
    }

    private void downloadResources(List<String> baseUrls) {
        for(ConfigurationResource resource : this.consulClientConfig.getResources() ) {
            downloadResource(baseUrls, resource);
        }
    }

    private void downloadResource(List<String> baseUrls, ConfigurationResource resource) {
        String path = resource.getPath();
        String file = resource.getFilename();

        int numberOfRetries = this.consulClientConfig.getRetry().getNumberOfRetries();
        int baseUrlsSize = baseUrls.size();

        for(int i = 0; i < numberOfRetries; i++) {
            String baseUrl = baseUrls.get(i % baseUrlsSize);
            ConsulClient client = new ConsulClient(baseUrl, this.consulClientConfig.getToken());

            try {
                logger.info("downloading resource from path {} to file {}", path, file);

                ConsulKvResponse response = client.kv().one(path);

                Files.write(Paths.get(file), response.getValue());

                // I am done
                break;
            } catch (ConsulException e) {
                logger.warn("Cannot fetch resource from path " + path, e);

                if(this.consulClientConfig.getRetry().isRetryWhenFailed()) {
                    sleep();
                } else {
                    throw new ConsulConfigurationException("Cannot download resource from path " + path);
                }
            } catch (IOException e) {
                throw new ConsulConfigurationException("Cannot write path " + path + " to file" + file, e);
            }
        }
    }

    private List<PropertySource<?>> loadPropertiesFile(String path, ConsulKvResponse response) throws IOException {
        PropertiesPropertySourceLoader sourceLoader = new PropertiesPropertySourceLoader();
        return sourceLoader.load(path, new ByteArrayResource(response.getValue()));
    }

    private List<PropertySource<?>> loadYmlFile(String path, ConsulKvResponse response) throws IOException {
        YamlPropertySourceLoader sourceLoader = new YamlPropertySourceLoader();
        return sourceLoader.load(path, new ByteArrayResource(response.getValue()));
    }

    private List<String> parseLocations(String locations) {
        if(locations == null || locations.isEmpty()) {
            return Collections.emptyList();
        }

        String[] parts = locations.split(CONST_LOCATIONS_SEPARATOR);

        return Arrays
                .stream(parts)
                .map(String::trim)
                .collect(Collectors.toList());
    }

    private List<String> parseBaseUrl(String baseUrls) {
        if(baseUrls == null || baseUrls.isEmpty()) {
            return Collections.emptyList();
        }

        String[] parts = baseUrls.split(CONST_BASE_PATHS_SEPARATOR);

        return Arrays
                .stream(parts)
                .map(String::trim)
                .collect(Collectors.toList());
    }

    private void sleep() {
        try {
            Thread.sleep(this.consulClientConfig.getRetry().getPauseBetweenRetries());
        } catch (InterruptedException e) {
            logger.trace("interrupted, e");
        }
    }
}
