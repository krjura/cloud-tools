package org.krjura.cloud.tools.consul.config;

public class ConsulConfigurationException extends RuntimeException {

    public ConsulConfigurationException(String s) {
        super(s);
    }

    public ConsulConfigurationException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
