package org.krjura.cloud.tools.consul.config.cloud;

import org.krjura.cloud.tools.consul.config.spring.ConsulClientConfig;
import org.krjura.cloud.tools.consul.config.ConsulClientPropertySourceLocator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.bootstrap.config.PropertySourceLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This will be used by spring cloud
 */
@Configuration
@ConditionalOnProperty(value = "org.krjura.cloud.tools.consul.config.enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass({PropertySourceLocator.class})
public class ConsulClientCloudAutoConfigure {

    @Bean
    public ConsulClientConfig consulClientConfig() {
        return new ConsulClientConfig();
    }

    @Bean
    public ConsulClientPropertySourceLocator bootstrapPropertySourceLocator(
            final ConsulClientConfig configConfiguration) {

        return new ConsulClientPropertySourceLocator(configConfiguration);
    }
}
