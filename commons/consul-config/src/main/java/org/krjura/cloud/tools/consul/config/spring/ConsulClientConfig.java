package org.krjura.cloud.tools.consul.config.spring;

import org.krjura.cloud.tools.consul.config.spring.pojo.ConsulClientRetryConfig;
import org.krjura.cloud.tools.consul.config.spring.pojo.ConfigurationResource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "org.krjura.cloud.tools.consul.config")
public class ConsulClientConfig {

    private String baseUrls = "http://localhost:85000";

    private String token;

    private String locations;

    private boolean enabled = true;

    private ConsulClientRetryConfig retry;

    private List<ConfigurationResource> resources;

    public ConsulClientConfig() {
        this.retry = new ConsulClientRetryConfig();
        this.resources = new ArrayList<>();
    }

    public String getBaseUrls() {
        return baseUrls;
    }

    public void setBaseUrls(String baseUrls) {
        this.baseUrls = baseUrls;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public ConsulClientRetryConfig getRetry() {
        return retry;
    }

    public void setRetry(ConsulClientRetryConfig retry) {
        this.retry = retry;
    }

    public List<ConfigurationResource> getResources() {
        return resources;
    }

    public void setResources(List<ConfigurationResource> resources) {
        this.resources = resources;
    }
}
