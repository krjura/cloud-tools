package org.krjura.cloud.tools.consul.config.spring.pojo;

public class ConfigurationResource {

    private String path;

    private String filename;

    public ConfigurationResource() {
        // mappers
    }

    public ConfigurationResource(String path, String filename) {
        this.path = path;
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
