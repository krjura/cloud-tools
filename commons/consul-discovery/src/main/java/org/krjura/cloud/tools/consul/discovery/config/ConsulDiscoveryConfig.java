package org.krjura.cloud.tools.consul.discovery.config;

import org.krjura.cloud.tools.consul.discovery.config.pojo.ConsulDiscoveryHealthConfig;
import org.krjura.cloud.tools.consul.discovery.config.pojo.ConsulDiscoveryRegisterConfig;
import org.krjura.cloud.tools.consul.discovery.config.pojo.ConsulDiscoveryRetryConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "org.krjura.cloud.tools.consul.discovery")
public class ConsulDiscoveryConfig {

    private String baseUrls = "http://localhost:8500";

    private String token;

    private boolean enabled = true;

    private ConsulDiscoveryRetryConfig retry;

    private ConsulDiscoveryRegisterConfig register;

    private ConsulDiscoveryHealthConfig health;

    public ConsulDiscoveryConfig() {
        this.retry = new ConsulDiscoveryRetryConfig();
        this.register = new ConsulDiscoveryRegisterConfig();
        this.health = new ConsulDiscoveryHealthConfig();
    }

    public String getBaseUrls() {
        return baseUrls;
    }

    public void setBaseUrls(String baseUrls) {
        this.baseUrls = baseUrls;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public ConsulDiscoveryRetryConfig getRetry() {
        return retry;
    }

    public void setRetry(ConsulDiscoveryRetryConfig retry) {
        this.retry = retry;
    }

    public ConsulDiscoveryRegisterConfig getRegister() {
        return register;
    }

    public void setRegister(ConsulDiscoveryRegisterConfig register) {
        this.register = register;
    }

    public ConsulDiscoveryHealthConfig getHealth() {
        return health;
    }

    public void setHealth(ConsulDiscoveryHealthConfig health) {
        this.health = health;
    }
}