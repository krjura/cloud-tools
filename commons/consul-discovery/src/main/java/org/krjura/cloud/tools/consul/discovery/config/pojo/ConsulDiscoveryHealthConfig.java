package org.krjura.cloud.tools.consul.discovery.config.pojo;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "org.krjura.cloud.tools.consul.discovery.health")
public class ConsulDiscoveryHealthConfig {

    private String name;

    private String id;

    private String interval;

    private String notes;

    private String deRegisterCriticalServiceAfter ;

    private List<String> args ;

    private String dockerContainerId;

    private String grpc;

    private boolean grpcUseTLS;

    private String http;

    private String method;

    private Map<String, String> header;

    private String timeout;

    private String tlsSkipVerify;

    private String ttl;

    private String serviceId;

    private String status;

    public ConsulDiscoveryHealthConfig() {
        this.args = new ArrayList<>();
        this.header = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDeRegisterCriticalServiceAfter() {
        return deRegisterCriticalServiceAfter;
    }

    public void setDeRegisterCriticalServiceAfter(String deRegisterCriticalServiceAfter) {
        this.deRegisterCriticalServiceAfter = deRegisterCriticalServiceAfter;
    }

    public List<String> getArgs() {
        return args;
    }

    public void setArgs(List<String> args) {
        this.args = args;
    }

    public String getDockerContainerId() {
        return dockerContainerId;
    }

    public void setDockerContainerId(String dockerContainerId) {
        this.dockerContainerId = dockerContainerId;
    }

    public String getGrpc() {
        return grpc;
    }

    public void setGrpc(String grpc) {
        this.grpc = grpc;
    }

    public boolean isGrpcUseTLS() {
        return grpcUseTLS;
    }

    public void setGrpcUseTLS(boolean grpcUseTLS) {
        this.grpcUseTLS = grpcUseTLS;
    }

    public String getHttp() {
        return http;
    }

    public void setHttp(String http) {
        this.http = http;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getTlsSkipVerify() {
        return tlsSkipVerify;
    }

    public void setTlsSkipVerify(String tlsSkipVerify) {
        this.tlsSkipVerify = tlsSkipVerify;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}