package org.krjura.cloud.tools.consul.discovery.config.pojo;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "org.krjura.cloud.tools.consul.discovery.register")
public class ConsulDiscoveryRegisterConfig {

    private String id;

    private String name;

    private List<String> tags;

    private String address;

    private Integer port;

    private Map<String, String> meta;

    private boolean enableTagOverride;

    public ConsulDiscoveryRegisterConfig() {
        this.tags = new ArrayList<>();
        this.meta = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Map<String, String> getMeta() {
        return meta;
    }

    public void setMeta(Map<String, String> meta) {
        this.meta = meta;
    }

    public boolean isEnableTagOverride() {
        return enableTagOverride;
    }

    public void setEnableTagOverride(boolean enableTagOverride) {
        this.enableTagOverride = enableTagOverride;
    }
}
