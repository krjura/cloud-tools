package org.krjura.cloud.tools.consul.discovery.ex;

public class ConsulDiscoveryException extends Exception {

    public ConsulDiscoveryException(String s) {
        super(s);
    }

    public ConsulDiscoveryException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
