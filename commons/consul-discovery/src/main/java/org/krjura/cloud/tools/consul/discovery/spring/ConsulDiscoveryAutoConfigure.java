package org.krjura.cloud.tools.consul.discovery.spring;

import org.krjura.cloud.tools.consul.discovery.config.ConsulDiscoveryConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ConditionalOnProperty(value = "org.krjura.cloud.tools.consul.discovery.enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass({DiscoveryClient.class})
@EnableScheduling
public class ConsulDiscoveryAutoConfigure {

    @Bean
    public ConsulDiscoveryConfig consulDiscoveryConfig() {
        return new ConsulDiscoveryConfig();
    }

    @Bean
    public ConsulDiscoveryRegistration consulDiscoveryRegistration(
            final ConsulDiscoveryConfig config,  final Environment environment) {

        return new ConsulDiscoveryRegistration(config, environment);
    }

}
