package org.krjura.cloud.tools.consul.discovery.spring;

import org.krjura.cloud.tools.consul.client.ConsulClient;
import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.pojo.agent.request.RegisterRequest;
import org.krjura.cloud.tools.consul.discovery.config.ConsulDiscoveryConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ConsulDiscoveryRegistration {

    private static final Logger logger = LoggerFactory.getLogger(ConsulDiscoveryRegistration.class);

    private static final String CONST_BASE_PATHS_SEPARATOR = ",";

    private final ConsulDiscoveryConfig config;

    private final Environment environment;

    private Map<String, String> registrations;

    private final Object lock = new Object();

    public ConsulDiscoveryRegistration(ConsulDiscoveryConfig config, Environment environment) {
        Objects.requireNonNull(config);
        Objects.requireNonNull(environment);

        this.config = config;
        this.environment = environment;
        this.registrations = new HashMap<>();
    }

    @Scheduled(fixedRate = 60000, initialDelay = 30000)
    public void register() {
        synchronized (lock) {
            registerToConsul(this.environment);
        }
    }

    @EventListener
    public void onApplicationStartup(ApplicationReadyEvent event) {
        logger.info("received ApplicationReadyEvent. Registering to consul");
        synchronized (lock) {
            registerToConsul(this.environment);
        }
    }

    @PreDestroy
    public void deRegister() {
        for(String baseUrl : this.registrations.keySet()) {
            String registrationId = this.registrations.get(baseUrl);

            ConsulClient client = new ConsulClient(this.config.getBaseUrls(), this.config.getToken());
            try {
                client.agent().deRegister(registrationId);
            } catch (ConsulException e) {
                logger.warn("Cannot de-register service with id of " + registrationId, e);
            }
        }
    }

    private void registerToConsul(Environment env) {
        RegisterRequest request = buildRequest(env);

        List<String> baseUrls = parseBaseUrl(this.config.getBaseUrls());

        for(String baseUrl : baseUrls) {
            // check if registration for some base url is already done
            if(this.registrations.containsKey(baseUrl)) {
                continue;
            }

            registerToConsul(request, baseUrl);
        }
    }

    private void registerToConsul(RegisterRequest request, String baseUrl) {
        int numberOfRetries = this.config.getRetry().getNumberOfRetries();

        for (int i = 0; i < numberOfRetries; i++) {

            try {
                ConsulClient client = new ConsulClient(baseUrl, this.config.getToken());

                logger.info("Trying to register this application to consul at {}", baseUrl);

                client.agent().register(request);
                this.registrations.put(baseUrl, request.getId());
                break;
            } catch (ConsulException e) {
                logger.warn("Discovery registration failed", e);

                if (this.config.getRetry().isRetryWhenFailed()) {
                    sleep();
                } else {
                    // since discovery is scheduled we can always do this latter on
                    logger.warn("Skipping discovery for consul at " + baseUrl + " due do failure", e);
                }
            }
        }
    }

    private RegisterRequest buildRequest(Environment env) {
        Integer envPort = env.getProperty("server.port", Integer.class, 80);
        String applicationName = env.getProperty("spring.application.name", UUID.randomUUID().toString());

        Integer port = getOrDefault(this.config.getRegister().getPort(), envPort);
        String id = getOrDefault(this.config.getRegister().getId(), getDefaultApplicationId(applicationName, port));
        String name = getOrDefault(this.config.getRegister().getName(), applicationName);
        String interval = getOrDefault(this.config.getHealth().getInterval(), "30s");

        return RegisterRequest
                .builder()
                .id(id)
                .name(name)
                .tags(this.config.getRegister().getTags())
                .address(this.config.getRegister().getAddress())
                .port(port)
                .meta(this.config.getRegister().getMeta())
                .enableTagOverride(this.config.getRegister().isEnableTagOverride())
                    .check()
                    .name(name)
                    .id(id)
                    .interval(interval)
                    .notes(this.config.getHealth().getNotes())
                    .deRegisterCriticalServiceAfter(this.config.getHealth().getDeRegisterCriticalServiceAfter())
                    .args(this.config.getHealth().getArgs())
                    .dockerContainerId(this.config.getHealth().getDockerContainerId())
                    .grpc(this.config.getHealth().getGrpc())
                    .grpcUseTLS(this.config.getHealth().isGrpcUseTLS())
                    .http(this.config.getHealth().getHttp())
                    .method(this.config.getHealth().getMethod())
                    .header(this.config.getHealth().getHeader())
                    .timeout(this.config.getHealth().getTimeout())
                    .tlsSkipVerify(this.config.getHealth().getTlsSkipVerify())
                    .ttl(this.config.getHealth().getTtl())
                    .serviceId(id)
                    .status(this.config.getHealth().getStatus())
                    .build()
                .build();
    }

    private String getDefaultApplicationId(String applicationName, Integer port) {
        return applicationName + "@" + this.config.getRegister().getAddress() + ":" + port;
    }

    private <T> T getOrDefault(T primary, T secondary) {
        return primary == null ? secondary : primary;
    }

    private List<String> parseBaseUrl(String baseUrls) {
        if(baseUrls == null || baseUrls.isEmpty()) {
            return Collections.emptyList();
        }

        String[] parts = baseUrls.split(CONST_BASE_PATHS_SEPARATOR);

        return Arrays
                .stream(parts)
                .map(String::trim)
                .collect(Collectors.toList());
    }

    private void sleep() {
        try {
            Thread.sleep(this.config.getRetry().getPauseBetweenRetries());
        } catch (InterruptedException e) {
            logger.trace("interrupted, e");
        }
    }
}
